
<?php

$this->load->view('Head');

?>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <form action="Login/cek_login" Method="POST">
          <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input class="form-control" id="username" name="username" type="text" aria-describedby="emailHelp" placeholder="Enter username" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control" id="password" name="password" type="password" placeholder="Password" required>
          </div>
          
          <button class="btn btn-primary btn-block" >Login</button>
          <div class="text-center">
          <a class="d-block small mt-3" href="<?php echo base_url(); ?>Register">Register an Account</a>
                        </div>
        </form>

      </div>
    </div>
  </div>

    <?php

    $this->load->view('Down');

    ?>
  </body>
